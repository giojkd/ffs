<?php

namespace App\Console\Commands;

use App\Models\Brandpage;
use App\Models\Product;
use Exception;
use Illuminate\Console\Command;
use Symfony\Component\HttpClient\HttpClient;
use Goutte\Client;

class ScrapeProductData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fs:scrapeproductdata';
    protected $baseUrl = 'https://www.farfetch.com';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $brandPages = Brandpage::whereNull('last_imported_at')->limit(10)->get();

        $this->line($brandPages->count());
        $successfulPages = 0;
        $brandPages->each(function($page,$key) use(&$successfulPages){

            $successfulImports = 0;
            $items = collect($page->brandpage_data->listingItems->items);

            $items->each(function($item,$key) use($page, &$successfulImports){

                $client = new Client(HttpClient::create(['timeout' => 120]));
                $url = implode([$this->baseUrl, $item->url]);
                $crawler = $client->request('GET', $url);
                $scripts = $crawler->filter('script');
                $this->line('Page '. $url . ' container '.$scripts->count().' scripts');
                try {

                    $script = $scripts->eq(24)->text();

                    if (!is_null($script)) {
                        $this->line('Found script 24');
                        $script = str_replace("window['__initialState_slice-pdp__'] = ", "", $script);
                        Product::create([
                            'brandpage_id' => $page->id,
                            'short_info' => $item,
                            'complete_info' => json_decode($script)
                        ]);
                        $successfulImports++;
                    }else{
                        $this->line('Coulnt\'t find script 24');
                    }
                } catch (Exception $e) {

                }

            });

            if($successfulImports == $items->count()){
                $page->last_imported_at = date('Y-m-d H:i:s');
                $page->save();
            }


        });
    }
}
