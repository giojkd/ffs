<?php

namespace App\Console\Commands;

use App\Models\Brand;
use Illuminate\Console\Command;
use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Goutte\Client;

class ScrapeBrandPages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fs:scrapebrandspages';

    protected $baseUrl = 'https://www.farfetch.com';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $brands = Brand::whereNull('brandpages_imported_at')->whereNotNull('brand_data')->get();
        $brands->each(function ($brand, $key) {
            $this->line($brand->name);
            $successfulPages = 0;
            $pagination = $brand->brand_data->listingPagination;
            for ($i = 1; $i <= $pagination->totalPages; $i++) {

                $url = implode('', [$this->baseUrl, $brand->url.'?page='.$i]);
                $client = new Client(HttpClient::create(['timeout' => 120]));
                $crawler = $client->request('GET', $url);
                $scripts = $crawler->filter('script');

                try {

                    $successfulPages++;
                    $script = $scripts->eq(24)->text();

                    if (!is_null($script)) {
                        $script = str_replace("window['__initialState_portal-slices-listing__'] = ", "", $script);
                        $brandData = json_decode($script);
                        $brand->brandpages()->create([
                            'page' => $i,
                            'brandpage_data' => $brandData
                        ]);

                        $this->line('Page '.$i.' status OK (1), sucessful pages so far '. $successfulPages);
                    }
                } catch (Exception $e) {

                }
            }

            if ($successfulPages == $pagination->totalPages) {
                $brand->brandpages_imported_at = date('Y-m-d H:i:s');
                $brand->save();
            }

        });
    }
}
