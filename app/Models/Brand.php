<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Brandpage;

class Brand extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $casts = ['brand_data' => 'object'];

    public function brandpages(){
        return $this->hasMany(Brandpage::class);
    }

}
