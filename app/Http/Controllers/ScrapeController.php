<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Exception;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Options;
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;



class ScrapeController extends Controller
{
    //
    protected $baseUrl = 'https://www.farfetch.com';



    public function scrapeBrands($locale, $gender)
    {
        $liBrandClasses = '._3d4abb._a99adf';
        $url = implode('/',[$this->baseUrl,$locale,'designers',$gender]);
        $dom = new Dom;
        $dom->loadFromUrl($url);
        $liBrands = $dom->find('li'. $liBrandClasses);
        $liBrands->each(function($brand, $key) use ($locale, $gender){
            $brandItem = $brand->find('a')[0];
            $brandObj = Brand::firstOrCreate([
                'name' => $brandItem->text,
                'gender' => $gender,
                'locale' => $locale,
                'url' => $brandItem->getAttribute('href')
            ]);
        });
    }


}

