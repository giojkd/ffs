<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brandpage extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $casts = ['brandpage_data' => 'object'];
}
