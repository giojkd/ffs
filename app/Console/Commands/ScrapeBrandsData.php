<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Brand;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Options;
use Goutte\Client;
use Exception;
use Symfony\Component\HttpClient\HttpClient;

class ScrapeBrandsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fs:scrapebrandsdata';

    protected $baseUrl = 'https://www.farfetch.com';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $brands = Brand::whereNull('brand_data')->whereNull('brand_data_scrape_status')->get();
        $this->line('There are still '.$brands->count().' brands to import');
        $countProcessedBrands = 0;
        $totalTime = 0;
        $successfulImports = 0;
        $failedImports = 0;
        $brands->each(function ($brand, $key) use(&$totalTime, &$countProcessedBrands, &$successfulImports, &$failedImports) {
            $startTime = microtime(true);
            $this->line($brand->name.' ('.$brand->gender.')');

            $url = implode('', [$this->baseUrl, $brand->url]);
            $client = new Client(HttpClient::create(['timeout' => 120]));
            $crawler = $client->request('GET', $url);
            $scripts = $crawler->filter('script');
            /*
            $scripts->each(function ($script, $key) {
                echo '<h3>' . $key . '</h3>';
                echo $script->text();
            });
*/

            try {
                $script = $scripts->eq(24)->text();

                if (!is_null($script)) {
                    $script = str_replace("window['__initialState_portal-slices-listing__'] = ", "", $script);
                    $brandData = json_decode($script);
                    $brand->brand_data = $brandData;
                    $brand->brand_data_scrape_status = 1;
                    $brand->save();
                    $this->line('Status OK (1)');
                    $successfulImports++;
                }
            } catch (Exception $e) {
                $brand->brand_data_scrape_status = 0;
                $brand->save();
                $this->line('Status KO (0)');
                $failedImports++;
            }
            $endTime = microtime(true);
            $spentTime = $endTime-$startTime;
            $totalTime += $spentTime;
            $countProcessedBrands++;
            $avgTimeS = number_format($totalTime / $countProcessedBrands,3);
            $spentTimeS = number_format($spentTime,3);
            $this->line('Took: '. $spentTimeS .'s avg: '. $avgTimeS.' scraped: '. $countProcessedBrands.' ok:'. $successfulImports.' ko: '. $failedImports);

        });
    }
}
