<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpClient\HttpClient;
use Goutte\Client;

class TestController extends Controller
{
    //
    protected $baseUrl = 'https://www.farfetch.com';

    public function index(){
        $client = new Client(HttpClient::create(['timeout' => 120]));
        $url = implode([$this->baseUrl, '/it/shopping/women/a-bathing-ape-vestito-modello-felpa-item-15576759.aspx?storeid=12667']);
        $crawler = $client->request('GET', $url);
        $scripts = $crawler->filter('script');
        $scripts->each(function($script,$key){
            echo '<h2>'.$key.'</h2>';
            echo $script->text();
        });

    }
}
